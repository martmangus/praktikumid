package praktikum7;

import praktikum6.Meetodid;

public class Arva2ra {

	public static void main(String[] args) {
		
		int arvutiArv = Meetodid.suvalineArv(1, 100);
		int arvamusteArv = 0;

		while (true) {
			int kasutajaArvab = Meetodid.kasutajaSisestus(1, 100);
			arvamusteArv++;
			if (kasutajaArvab == arvutiArv) {
				System.out.println("Arvasid ära, päris osav oled!");
				System.out.println("Kokku proovisid " + arvamusteArv + " korda.");
				break;
			} else if (arvutiArv > kasutajaArvab) {
				System.out.println("Arvuti mõeldud arv on suurem.");
			} else /* if (arvutiArv < kasutajaArvab) */ {
				System.out.println("Arvuti mõeldud arv on väiksem.");
			} 
		}
	}
	
}
