package praktikum7;

import praktikum6.Meetodid;

public class KullKiriPanustega {

	public static void main(String[] args) {
		
		int kasutajaRaha = 100;
		
		while (kasutajaRaha > 0) {		
			System.out.println("Sul on: " + kasutajaRaha + " raha");
			int maxPanus = Math.min(25, kasutajaRaha);
			int panus = Meetodid.kasutajaSisestus("Palun sisesta panus (maksimaalselt " + maxPanus + ")", 1, maxPanus);
			kasutajaRaha -= panus;
			
			int myndiVise = Meetodid.suvalineArv(0, 1);	// 0 - kull, 1 - kiri
			if (1 == myndiVise) {
				System.out.println("Tuli kiri, saad raha topelt tagasi!");
				kasutajaRaha += panus * 2;
			} else {
				System.out.println("Tuli kull, ei saa midagi.");
			}
		}
		System.out.println("Raha otsas, mäng läbi!");
		
	}
	
}
