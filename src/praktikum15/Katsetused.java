package praktikum15;

public class Katsetused {

	public static void main(String[] args) {
		
		String tekst = new String("Tere!");
		
		Punkt p1 = new Punkt(1, 3);
		Punkt p2 = new Punkt(5, 7);
		System.out.println(p1);
		
		Joon j = new Joon(p1, p2);
		System.out.println(j);
		System.out.println("Joone pikkus on: " + j.pikkus());

		Ring r = new Ring(p1, 12.3);
		System.out.println(r);
		System.out.println("Ringi pindala on: " + r.pindala());
		System.out.println("Ringi ümbermõõt on: " + r.ymberm66t());
		
		Silinder s = new Silinder(r, 23.56);
		System.out.println();
		
	}

}
