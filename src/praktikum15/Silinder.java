package praktikum15;

public class Silinder extends Ring {

	double k6rgus;
	
	public Silinder(Ring r, double h) {
		super(r.keskpunkt, r.raadius);
		k6rgus = h;
	}
	
	@Override
	public String toString() {
		return "Silinder, noh";
	}
	
	public double pindala() {
		return super.pindala() * 2 + ymberm66t() * k6rgus;
	}
	
	public double ruumala() {
		return super.pindala() * k6rgus;
	}

}
