package praktikum15;

public class Joon {

	Punkt algus, l6pp;
	
	public Joon(Punkt p1, Punkt p2) {
		algus = p1;
		l6pp = p2;
	}
	
	@Override
	public String toString() {
		return "Joon(" + algus + " - " + l6pp +")";
	}

	public double pikkus() {
		double a = l6pp.x - algus.x;
		double b = l6pp.y - algus.y;
		return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
	}
	
}
