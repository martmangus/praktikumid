package praktikum5;

import lib.TextIO;

public class Tabel22ristega {

	public static void main(String[] args) {
		
		System.out.println("Palun sisesta tabeli suurus");
		int tabeliSuurus = TextIO.getlnInt();
		
		tryki22Ris(tabeliSuurus);
		
		for (int i = 0; i < tabeliSuurus; i++) {
			System.out.print("| ");
			for (int j = 0; j < tabeliSuurus; j++) {
				if (j == i || i + j == tabeliSuurus - 1) {
					System.out.print("x ");
				} else {
					System.out.print("0 ");
				}
				//System.out.print("(i:" + i + " j:" + j + ") ");
			}
			System.out.print("|");
			System.out.println();
		}
		
		tryki22Ris(tabeliSuurus);
	}

	private static void tryki22Ris(int tabeliSuurus) {
		for (int i = 0; i < tabeliSuurus * 2 + 3; i++) {
			System.out.print("-");
		}
		System.out.println();
	}

}
