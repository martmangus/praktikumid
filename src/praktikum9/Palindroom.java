package praktikum9;

import lib.TextIO;
import praktikum7.TagurpidiS6na;

public class Palindroom {

	public static void main(String[] args) {

		System.out.println("Palun sisesta sõna");
		String s6na = TextIO.getlnString();
		
		if (onPalindroom(s6na)) {
			System.out.println("See sõna on palindroom");
		} else {
			System.out.println("Tegemise ei ole palindroomiga");
		}
	}

	public static boolean onPalindroom(String s6na) {
		return s6na.equals(TagurpidiS6na.tagurpidi(s6na));
	}

}
